import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostService} from "../shared/services/post.service";
import {Post} from "../../shared/interfaces";
import {Subscription} from "rxjs";
import {AlertService} from "../shared/services/alert.service";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  postSub: Subscription;
  delSub: Subscription;
  search: string = "";


  constructor(private postService: PostService,
              private alert: AlertService) { }

  ngOnInit(): void {
    this.postSub = this.postService.getAll().subscribe(posts => {
      this.posts = posts

    }
    )
  }

  ngOnDestroy() {
    if (this.postSub) {
      this.postSub.unsubscribe()
    }
    if(this.delSub) {
      this.delSub.unsubscribe()
    }
  }

  remove(id: string) {
    this.postService.remove(id).subscribe(() => {
      this.posts.filter(post => post.id !== id)
      this.alert.warning("Пост был удален")
    })
  }
}
