import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable, pipe} from "rxjs";
import {FBCreateResponse, Post} from "../../../shared/interfaces";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class PostService {
  constructor(private http: HttpClient) {
  }
  create(post: Post): Observable<Post> {
    return this.http.post(`${environment.fbDbUrl}posts.json`, post)
      .pipe(
        map((response: FBCreateResponse) => {
          return {
            ...post,
            id: response.name,
            date: new Date(post.date)
          }
        })
      )
  }

  getAll(): Observable<Post[]> {
    return this.http.get(`${environment.fbDbUrl}posts.json`)
      .pipe(
        map((resp:{[key:string]: any}) => {
          return Object.keys(resp).map(item => ({
            ...resp[item],
            id: item,
            date: new Date(resp[item].date)
          }))
        })
      )
  }

  remove(id:string): Observable<void> {
    return this.http.delete<void>(`${environment.fbDbUrl}posts/${id}.json`)
  }

  getById(id: string): Observable<Post> {
    return this.http.get<Post>(`${environment.fbDbUrl}posts/${id}.json`)
      .pipe(
        map((post: Post) => {
          return {
            ...post, id,
            date: new Date(post.date)
          }
        })
      )
  }

  update(post: Post): Observable<Post> {
    return this.http.patch<Post>(`${environment.fbDbUrl}posts/${post.id}.json`, {})
  }
}
